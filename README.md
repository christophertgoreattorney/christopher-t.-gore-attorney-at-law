I am a Houston criminal defense lawyer, dedicated to serving my clients. Together we will develop a strategy that best suits your specific needs. I give you the individual care, detailed preparation, and, most importantly, the aggressive execution that your case deserves.

Address: 1401 Richmond Ave, #240, Houston, TX 77006, USA

Phone: 713-223-1600

Website: https://ctgore.com
